﻿using UnityEngine;
using System.Collections;

public class penguinArrows : MonoBehaviour {
    
    //float variable which controls the speed
    public float penguinspeed=5f;

    public float penguinspeed2 = 6f;

    public string myname = "";
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    //arrow keys value
        float arrowKey = Input.GetAxis("Horizontal");
        //arrowkey value is a range from -1 to +1;

        //move penguin left or right
        transform.Translate(Vector3.right * arrowKey * penguinspeed * Time.deltaTime);



	}
}
