﻿using UnityEngine;
using System.Collections;

public class penguinController : MonoBehaviour {

    //sets the speed of the penguin
    public float penguinSpeed;
    public float rotationSpeed;

    int score;
    


    //hit something, other is a reference to the other object
    void OnTriggerEnter2D(Collider2D other)
    {
       // Debug.Log("hit wall");
        transform.position = new Vector3(0, 0, 0);
       // GameObject.FindGameObjectWithTag("walls").GetComponent<Camera>(). += new Vector3(-10f,0);
       //added a comment 

    }


    void OnGUI()
    {
       //create a label for the score
        GUI.color = Color.green;
        GUI.Label(new Rect(0, 0, 150, 100), "Score :" + score);
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
       // Debug.Log(transform.position);
        //move the penguin up and down
        transform.Translate(Vector3.up * Input.GetAxis("Vertical") * penguinSpeed * Time.deltaTime);
       
        //rotate the penguin in the forward direction
        transform.Rotate(Vector3.forward * Input.GetAxis("Horizontal") * -rotationSpeed * Time.deltaTime);
	}
}
