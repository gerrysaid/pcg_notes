﻿using UnityEngine;
using System.Collections;

public class CubeController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 mousePos = Input.mousePosition;

		Vector3 mouseScreenPoint = Camera.main.ScreenToWorldPoint
			(mousePos);
	
		transform.LookAt (mouseScreenPoint,Vector3.forward);
		
		transform.eulerAngles = new Vector3 
			(0, 0, -transform.eulerAngles.z);
	}
}
