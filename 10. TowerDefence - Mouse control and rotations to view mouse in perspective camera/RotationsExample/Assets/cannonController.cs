﻿using UnityEngine;
using System.Collections;

public class cannonController : MonoBehaviour {

	public GameObject bullet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetMouseButtonDown (0)) {
			//create a bullet slightly in front 
			//of the cannon
			Instantiate(bullet,
			   transform.position,transform.rotation);
		}
	}
}
