﻿using UnityEngine;
using System.Collections;

public class muzzleController : MonoBehaviour {

	public GameObject bullet;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		//if I left click
		if (Input.GetMouseButtonDown (0)) {
			GameObject temp = (GameObject)Instantiate(bullet,
			            transform.position,
			            transform.rotation);
			temp.GetComponent<Rigidbody2D>().AddForce (temp.transform.up * 25f * Time.deltaTime,ForceMode2D.Force);
		}
	}
}
