﻿using UnityEngine;
using System.Collections;

public class physicsTurretController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 mousePos = Input.mousePosition;
		
		Vector3 mouseWorldPos = Camera.main.
			ScreenToWorldPoint (mousePos);
		
		transform.LookAt (mouseWorldPos,Vector3.forward);

		transform.eulerAngles = new Vector3 (
			0f, 0f, Mathf.Clamp (-transform.eulerAngles.z, -90f, 0f));


	}
}
