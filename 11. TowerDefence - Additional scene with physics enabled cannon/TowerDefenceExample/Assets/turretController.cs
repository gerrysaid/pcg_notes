﻿using UnityEngine;
using System.Collections;

public class turretController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

	bool move = false;
	// Update is called once per frame
	void Update () {
		Vector3 mousePos = Input.mousePosition;

		Vector3 mouseWorldPos = Camera.main.
			ScreenToWorldPoint (mousePos);

		transform.LookAt (mouseWorldPos,Vector3.forward);

		//360 degree rotation
		transform.eulerAngles = new Vector3
			(0f, 0f, -transform.eulerAngles.z);

		//clamped to the right
		//transform.eulerAngles = new Vector3 (
		//	0f, 0f, Mathf.Clamp (-transform.eulerAngles.z, -180f, 180f));

		//if I right click, LERP to the target
		if (Input.GetMouseButtonDown (1)) {
			move = !move;

		}

		Debug.Log (move);

		if (move) {
			Vector3 targetPos = new Vector3(mouseWorldPos.x,mouseWorldPos.y,0f);
			transform.position = Vector3.Lerp (transform.position,targetPos,5f*Time.deltaTime);
			if (Vector3.Distance(transform.position,targetPos)<0.2f)
			{
				move = false;
			}
		}


	}
}
