﻿using UnityEngine;
using System.Collections;

public class robotController : MonoBehaviour {

	Animator anim;
	BoxCollider2D myCollider;

	// Use this for initialization
	void Start () {
		anim = this.GetComponent<Animator> ();
		myCollider = this.GetComponent<BoxCollider2D> ();

	}
	
	// Update is called once per frame
	void Update () {
		anim.SetBool ("canCrouch",false);
		anim.SetBool ("canDance",false);
		myCollider.size = new Vector3(0.49f,0.61f);
		if (Input.GetKey (KeyCode.C)) {
			anim.SetBool ("canCrouch",true);
			myCollider.size = new Vector3(0.5f,0.5f);
		}

		if (Input.GetKey (KeyCode.D)) {
			anim.SetBool ("canDance",true);
		}

	}
}
