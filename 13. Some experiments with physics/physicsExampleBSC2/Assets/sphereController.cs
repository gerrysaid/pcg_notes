﻿using UnityEngine;
using System.Collections;

public class sphereController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody2D>().AddRelativeForce (Vector3.down * 2f, ForceMode2D.Impulse);
	}

	void OnCollisionEnter2D(Collision2D theCollision)
	{
		//the tag of the object that I hit
		if (theCollision.gameObject.tag == "othersphere") {
			Destroy(theCollision.gameObject);
		}

		if (theCollision.gameObject.tag == "othersphere2") {
			theCollision.gameObject.GetComponent<Rigidbody2D>().AddRelativeForce(Vector3.left * 2f,ForceMode2D.Impulse);
		}
	}

	//waits for all physics operations to complete
	void FixedUpdate()
	{
		if (Input.GetKeyDown (KeyCode.Space)) {
			GetComponent<Rigidbody2D>().AddRelativeForce (Vector3.up * 2f,ForceMode2D.Impulse);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
