﻿using UnityEngine;
using System.Collections;
using Mono.Data.Sqlite; 
using System.Data; 
using System;

public class dbScript : MonoBehaviour {

	string names = "";


	void OnGUI()
	{
		GUI.Label (new Rect (0, 0, 500, 100), names);
	}

	// Use this for initialization
	void Start () {
		string conn = "URI=file:" + Application.dataPath + "/mydb.s3db"; //Path to database.
		Debug.Log (conn);
		IDbConnection dbconn;
		dbconn = (IDbConnection) new SqliteConnection(conn);
		dbconn.Open(); //Open connection to the database.
		IDbCommand dbcmd = dbconn.CreateCommand();

		string sqlQuery = "SELECT firstName,secondName, id " + "FROM Player";
		dbcmd.CommandText = sqlQuery;
		IDataReader reader = dbcmd.ExecuteReader();
		
		
		while (reader.Read())
		{
			string firstName = reader.GetString(0);
			string secondName = reader.GetString(1);
			int id = reader.GetInt32(2);
			names += "\n id= "+id+"  name ="+firstName+"  secondName ="+  secondName;
			Debug.Log( "id= "+id+"  name ="+firstName+"  secondName ="+  secondName);
		}
		
		reader.Close();
		reader = null;
		dbcmd.Dispose();
		dbcmd = null;
		dbconn.Close();
		dbconn = null;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
