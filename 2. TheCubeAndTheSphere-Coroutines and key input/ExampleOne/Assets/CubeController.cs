﻿using UnityEngine;
using System.Collections;

public class CubeController : MonoBehaviour {

    public GameObject spherePrefab;

    //generate 3 spheres by default
    public int numberOfSpheres;

    public float speedX; //Please remember to write these 
    public float speedY; // ... in camelCase 

    public GUISkin theme;
    public GUISkin theme2;

    public GUISkin[] skins;

    int score;
    //preset lives at 3
    int lives = 3;

    //countdown timer
    int countdown = 5;

	// Use this for initialization
    void Start()
    {
        StartCoroutine(countDown());
        StartCoroutine(moveCube());
        StartCoroutine(generateSpheres());

    }

    IEnumerator generateSpheres()
    {
        //loop for numberofspheres times
        for (int i = 0; i < numberOfSpheres; i++)
        {
            Instantiate(spherePrefab, generateRandomPosition(), Quaternion.identity);
            yield return new WaitForSeconds(1f);
        }
    }

    //create a coRoutine
    IEnumerator countDown()
    {
        while (countdown > 0)
        {
            yield return new WaitForSeconds(1f);
            countdown--;
        }
    }

    IEnumerator moveCube()
    {
        //keep the coroutine running forever
        while (true)
        {
            yield return new WaitForSeconds(5f);
            transform.position = generateRandomPosition();
        }
    }

    Vector3 generateRandomPosition()
    {
        float randomX = Random.Range(-4.5f,4.5f);
        float randomY = Random.Range(-4.5f,4.5f);
        return new Vector3(randomX, randomY, 0);
    }
  

    void OnGUI()
    {
        GUI.skin = theme;
        GUI.Label(new Rect(0, 0, 150, 100), "Score : " + score +"\nLives : "+lives);
        if (countdown > 0)
        {
            GUI.Label(new Rect(Screen.width / 2 - 25, Screen.height / 2 - 25, 50, 50), countdown.ToString());
        }
    }

	// Update is called once per frame
	void Update () {
        //added countdown as a condition
        if ((lives > 0) && (countdown == 0))
        {
            //if I press escape, takes me to the menu
            if (Input.GetKey(KeyCode.Escape))
            {
                Application.LoadLevel(0);
            }
            transform.Translate(Vector3.up * Time.deltaTime * speedY * Input.GetAxis("Vertical"));
            transform.Rotate(Vector3.back * Time.deltaTime * speedX * Input.GetAxis("Horizontal"));
        }
        
        if (lives == 0)
        {
            //itfaw fil-menu meta jmut
            Application.LoadLevel(0);
        }
	}

    //onTriggerEnter2D method to do collisions
    void OnTriggerEnter2D(Collider2D otherObjectCollider)
    {
        if (otherObjectCollider.gameObject.tag == "wall")
        {
            //if I hit the wall, I go back to the middle of the screen
            lives--;
            transform.position = new Vector3(0, 0, 0);
        }

        if (otherObjectCollider.gameObject.tag == "circle")
        {
            //will 'eat' the circle
            
            
            //destroy the circle
            Destroy(otherObjectCollider.gameObject);
            
            
            Vector3 pos;

            //check if the random is the same position as the cube
            //and regenerate the random if it is
            do
            {
                pos = generateRandomPosition();
            } while (transform.position == pos);

            score++;

           // otherObjectCollider.gameObject.transform.position = pos;
                
        }

    }
}


	