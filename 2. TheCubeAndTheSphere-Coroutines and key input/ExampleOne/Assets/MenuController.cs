﻿using UnityEngine;
using System.Collections;

public class MenuController : MonoBehaviour {
    
    public GUISkin textSkin;

    //allows us to create GUI elements
    void OnGUI()
    {
        GUI.color = Color.red;

        GUI.skin = textSkin;

        //Rect with parameters x,y,width,height and a string at the end with the text
        GUI.Label(new Rect(Screen.width / 2 -100, Screen.height / 2 - 50, 200, 100), "The Cube & Sphere");

        if (GUI.Button(new Rect(Screen.width / 2 - 50, (Screen.height / 2 - 50)+120, 100, 100), "Play!"))
        {
            //load the main game scene
            Application.LoadLevel(1);
        }

    }
	
}
