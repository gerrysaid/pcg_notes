﻿using UnityEngine;
using System.Collections;

public class cubePositionScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}


    float edgeX;
    float edgeY;
	// Update is called once per frame
	void Update () {
        //for borders

        //what is the world equivalent of the screen.width?
        edgeX = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x;

        //what is the world equivalent of the screen height?
        edgeY = Camera.main.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y;

        if ((transform.position.x < edgeX) && (transform.position.x > -edgeX))
        {
            transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * 20f * Time.deltaTime);
        }
        else { transform.position = new Vector3(0, 0, 0); }

        if ((transform.position.y < edgeY && transform.position.y > -edgeY))
        {
            transform.Translate(Vector3.up * Input.GetAxis("Vertical") * 20f * Time.deltaTime);
        }
        else { transform.position = new Vector3(0,0,0); }


	}

    void OnGUI()
    {
        //write the coordinates of the cube in WORLD coordinates
        GUI.Label(new Rect(0, 0, 200, 35), "Position WORLD: " + transform.position.ToString());

        //write the coordinates of the cube in SCREEN coordinates
        GUI.Label(new Rect(0, 35, 200, 35), "Position SCREEN: " + Camera.main.WorldToScreenPoint(transform.position).ToString());

        //screen edges in WORLD coordinates
        GUI.Label(new Rect(0, 70, 200, 35), "Edge X:" + edgeX + " Y:" + edgeY);

        //size of screen
        GUI.Label(new Rect(0, 95, 200, 35), "Screen width:" + Screen.width + " Height:" + Screen.height);

    }

}
