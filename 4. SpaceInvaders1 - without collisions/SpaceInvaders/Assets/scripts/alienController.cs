﻿using UnityEngine;
using System.Collections;

public class alienController : MonoBehaviour {


    public GameObject alienLaser;


    IEnumerator shootLaser()
    {
        //infinite loop (alien keeps shooting until killed)
        while (true)
        {
            GameObject tempLaser = (GameObject)Instantiate(alienLaser, transform.position, Quaternion.identity);
            tempLaser.GetComponent<laserController>().moveDown = true; //so it moves down
            yield return new WaitForSeconds(Random.Range(1f,3f));
        }
    }
    
    // Use this for initialization
	void Start () {
        //start shooting.
        StartCoroutine(shootLaser());
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
