﻿using UnityEngine;
using System.Collections;

public class gameController : MonoBehaviour {

    void OnGUI()
    {
        int mylives;

        mylives = GameObject.FindGameObjectWithTag("spaceship").GetComponent<spaceshipController>().lives;

        GUI.Label(new Rect(0, 0, 150, 35), "Lives :" + mylives);
    }

    public Vector3 getScreenEdges(float margin)
    {
        //this returns a vector 3 with the WORLD coordinates of the top right of the screen
        return Camera.main.ScreenToWorldPoint(new Vector3(Screen.width-margin,Screen.height-margin,0));
    }


}
