﻿using UnityEngine;
using System.Collections;

public class laserController : MonoBehaviour {

    public bool moveDown;
    
	
	// Update is called once per frame
	void Update () {
        //if move down is false, it will move up
        if (moveDown)
            transform.Translate(Vector3.down * 15f * Time.deltaTime);
        else
            transform.Translate(Vector3.up * 15f * Time.deltaTime);

	}


    //this function is triggered when an object leaves all cameras
    void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    
}
