﻿using UnityEngine;
using System.Collections;

public class spaceshipController : MonoBehaviour {
    float edgeX;

    public GameObject laser;
    public int lives;

	// Use this for initialization
	void Start () {
        //get the controller 
        controller = GameObject.FindGameObjectWithTag("gamecontroller").GetComponent<gameController>();
	}

    //variable for the game controller
    gameController controller;
	
	// Update is called once per frame
	void Update () {
	    
        

        //margin of ten pixels passed as a parameter.  The margins are therefore 10px in for the spaceship
        edgeX = controller.getScreenEdges(10f).x;

        if ((transform.position.x <= edgeX) && (transform.position.x >= -edgeX))
        {
            transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * 20f * Time.deltaTime);
        }

        //stop at the edge
        if (transform.position.x >= edgeX)
        {
            transform.position = new Vector3(edgeX, transform.position.y, 0);
        }

        //stop at the other edge
        if (transform.position.x <= -edgeX)
        {
            transform.position = new Vector3(-edgeX, transform.position.y, 0);
        }

        //if I pressed the space bar
        if (Input.GetKeyDown(KeyCode.Space))
            Instantiate(laser, transform.position, Quaternion.identity);


	}
}
