﻿using UnityEngine;
using System.Collections;

public class swarmController : MonoBehaviour {

    public float rows;
    public float columns;
    public float spacing;
    public GameObject alien;

    float swarmwidth;

    gameController controller;
    Vector3 screenEdge;
    float direction=1;

    void createAliens()
    {
        //the current position of the swarm
        Vector3 swarmPosition = transform.position;

        for (float r = 0; r > -rows; r--)
        {
            //for each column
            for (float c = 0; c < columns; c++)
            {
                GameObject tempAlien = (GameObject)Instantiate(alien, (new Vector3(0, 0, 0)), Quaternion.identity);
                tempAlien.transform.parent = this.gameObject.transform;
                float alienwidth = tempAlien.transform.localScale.x / 2;
                float alienheight = tempAlien.transform.localScale.y / 2;

                float xpos = c + (alienwidth) + c * spacing;
                float ypos = r - (alienheight) - r * -spacing;

                tempAlien.transform.localPosition = new Vector3(xpos, ypos, 0);
                tempAlien.gameObject.name = r + "x" + c;
                swarmwidth = xpos;
            }
        }
            swarmwidth += (alien.transform.localScale.x / 2);
            //write the width of the entire swarm in the console
            Debug.Log(swarmwidth);
       
    }


    

    // Use this for initialization
    void Start()
    {
        createAliens();   
        
        controller = GameObject.FindGameObjectWithTag("gamecontroller").GetComponent<gameController>();

        screenEdge = controller.getScreenEdges(2f);

        //top left corner
        transform.position = new Vector3(-screenEdge.x, screenEdge.y, 0);
	    
	}

  
	// Update is called once per frame
	void Update () {
        


        if (transform.position.x > screenEdge.x - swarmwidth)
        {
            Debug.Log("Right hand side");
            direction = -1;
        }

        if (transform.position.x < -screenEdge.x)
        {
            Debug.Log("Left hand side");
            direction = 1;
        }

        transform.Translate(Vector3.right * 5f * direction * Time.deltaTime);
        
	
	}
}
