﻿using UnityEngine;
using System.Collections;

public class alienLaserController : MonoBehaviour {

	
	// Update is called once per frame
	void Update () {
        //move down
        if (transform.position.y >= -Camera.main.orthographicSize)
        {
            //move the laser down
            transform.Translate(Vector3.down * 15f * Time.deltaTime);
        }
        else
        {
            //remove the laser from memory
            Destroy(this.gameObject);
        }

	}
}
