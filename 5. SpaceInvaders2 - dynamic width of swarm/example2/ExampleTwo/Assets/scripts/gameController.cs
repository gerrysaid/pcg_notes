﻿using UnityEngine;
using System.Collections;

public class gameController : MonoBehaviour {

    public int score;
    public int lives;


    public int maxhorizontal;
    public int maxvertical;

    public int minhorizontal;
    public int minvertical;

    public Vector3 calculateEdge()
    {
        //I know the screen width
       // Debug.Log(Screen.width);

        Vector3 screenPointEdge = new Vector3(Screen.width, Screen.height, 0);

        return Camera.main.ScreenToWorldPoint(screenPointEdge);
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //display score and lives on screen
    void OnGUI()
    {
        GUI.color = Color.red;
        GUI.Label(new Rect(0, 0, 150, 30), "Score :" + score);
        GUI.Label(new Rect(0, 30, 150, 30), "Lives :" + lives);
    }


}
