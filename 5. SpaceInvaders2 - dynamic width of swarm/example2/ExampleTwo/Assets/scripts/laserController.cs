﻿using UnityEngine;
using System.Collections;

public class laserController : MonoBehaviour {

	
	
	// Update is called once per frame
	void Update () {
	    //move up
        if (transform.position.y <= Camera.main.orthographicSize)
        {
            //move the laser up
            transform.Translate(Vector3.up * 15f * Time.deltaTime);
        }
        else
        {
            //remove the laser from memory
            Destroy(this.gameObject);
        }

	}

    gameController controller;
    myswarmController scontroller;

    //when the laser hits something
    void OnTriggerEnter2D(Collider2D other)
    {
        //if laser hits an alien
        if (other.gameObject.tag == "alien")
        {
            controller = GameObject.FindGameObjectWithTag("GameController")
                .GetComponent<gameController>();
            //increase the score by 1
            controller.score++;

            //get the row
            int alienrow =int.Parse(
                other.name.Substring(0,other.name.IndexOf("x")));

            //get the column
            int aliencolumn = int.Parse(
                other.name.Substring(other.name.IndexOf("x")+1, 1));

            
            
            Destroy(other.gameObject);
           

            bool recalculate=true;
            int aliencounter = 0;
            int r = 0;
            //the outer edge
            if ((aliencolumn == controller.minvertical) || (aliencolumn == controller.maxvertical)) 
            {
                recalculate = true;
                aliencounter = 0;

                for (r = 0; r <= controller.maxvertical; r++)
                {
                    
                    if (findAlien(r, aliencolumn))
                    {

                        Debug.Log("found alien"+r+" "+aliencolumn);
                        aliencounter++;
                        //if only one alien left
                        if (aliencounter>1)
                            recalculate = false;
                    }
                }

                //1 alien found
                if (recalculate)
                {
                   // Debug.Log("recalculate");
                    //recalculate widths and change maxvertical or minvertical
                    
                    if ((aliencolumn == controller.minvertical) && (controller.minvertical <= controller.maxvertical))
                    {
                   
                        controller.minvertical++;
                   
                    }
                    
                    if ((aliencolumn == controller.maxvertical) && (controller.maxvertical > 0))
                    {
                        
                        
                        
                        controller.maxvertical--;
                    }
                    Debug.Log("Min: "+controller.minvertical+ " Max:"+controller.maxvertical);

                    scontroller = GameObject.FindGameObjectWithTag("swarm").GetComponent<myswarmController>();
                    scontroller.recalcMargins();


                }
               
                
             
                //recalculate width

            }


      
            Destroy(this.gameObject);

        }
    }

    bool findAlien(int row, int column)
    {
        if (GameObject.Find(row + "x" + column) != null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
