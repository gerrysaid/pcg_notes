﻿using UnityEngine;
using System.Collections;

public class myswarmController : MonoBehaviour {

    gameController controller;

    

    public float xpos;

    //reference to the alien
    public GameObject alien;

    public int columns;
    public int rows;

    public float spacing;

    Vector3 topRight;

	// Use this for initialization
	void Start () {
        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<gameController>();

        topRight = controller.calculateEdge();

        transform.position = new Vector3(-topRight.x, topRight.y, 0);
	
        //creating the aliens
        canMove = false;

        StartCoroutine(createAliens());

	}
    float alienwidth;

    GameObject tempAlien;

    bool canMove;



    float alienheight;
    float inneroffset=0;

    public void recalcMargins()
    {
        Debug.Log("recalc margins("+controller.minvertical + " "+controller.maxvertical+")");

        inneroffset = (controller.minvertical * (alien.transform.localScale.x + spacing));

        
      
        //calculate width, by moving it inwards
        alienwidth = (controller.maxvertical * (alien.transform.localScale.x + spacing)) + alien.transform.localScale.x;
        Debug.Log("offsets(" + inneroffset + " " + alienwidth + ")");
        Debug.Log("margins(" + (-topRight.x - inneroffset) + " " + (topRight.x - alienwidth) + ")");
    }

    IEnumerator createAliens()
    {
        //gameobject comes in the TOP LEFT of the object I created
        for (int rowcounter = 0; rowcounter < rows; rowcounter++)
        {
            for (int columncounter = 0; columncounter < columns; columncounter++)
            {
                tempAlien = (GameObject)Instantiate(alien, new Vector3(0, 0, 0),
                    Quaternion.identity);

                alienwidth = columncounter *
                    (tempAlien.transform.localScale.x + spacing);

                //the height I need to move down
                alienheight = rowcounter *
                    (tempAlien.transform.localScale.y + spacing);

                Vector3 startingPosition =
                    new Vector3((tempAlien.transform.localScale.x / 2) + alienwidth,
                    -(tempAlien.transform.localScale.y / 2)-alienheight, 0);

                tempAlien.transform.parent = this.transform;

                //localPosition is relative to the parent object, while 
                //POSITION is relative to the normal object
                tempAlien.transform.localPosition = startingPosition;
                
                tempAlien.name = rowcounter + "x" + columncounter;


                yield return null;
                //create an alien every second
              //  yield return new WaitForSeconds(1f);
            }
        }

        //maximum index of the aliens at the moment
        controller.maxhorizontal = rows - 1;
        controller.maxvertical = columns - 1;

   

        controller.minhorizontal = 0;
        controller.minvertical = 0;

         alienwidth += tempAlien.transform.localScale.x;
        
       // Debug.Log(alienwidth);
        canMove = true;
        yield return null;
    }

    //aliens start moving right
    int dir=1;
	// Update is called once per frame
	void Update () {
       
        
        if (canMove)
        {
            //travelling right, since our swarm controller
            //is on the top left, he will stop correctly
            if (transform.position.x < -topRight.x-inneroffset)
            {
                dir = 1;
            }

            if (transform.position.x > topRight.x - alienwidth)
            {
                dir = -1;
            }

            transform.Translate(Vector3.right * 5f * dir * Time.deltaTime);
        }
	}
}
