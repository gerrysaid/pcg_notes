﻿using UnityEngine;
using System.Collections;

public class spaceshipController : MonoBehaviour {

    public GameObject laser;

	// Use this for initialization
	void Start () {
	
	}

    //a variable of type gameController
    gameController controller;


	// Update is called once per frame
	void Update () {

        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<gameController>();

        Vector3 edge = controller.calculateEdge();
        //move spaceship horizontally - 20f is a hardcoded speed variable
        
        //so it hits the edge not the middle of the cube
        edge.x -= 0.5f;

        //if statements that keep the spaceship on screen
        if ((transform.position.x >= -edge.x) && (transform.position.x <= edge.x))
                transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * 20f * Time.deltaTime);
	    
        //Like this it sticks to the sides.  Solve the bug
        //bug solved
        if (transform.position.x < -edge.x)
            transform.position = new Vector3(-edge.x,transform.position.y,0);

        if (transform.position.x > edge.x)
            transform.position = new Vector3(edge.x, transform.position.y, 0);

        //creates an instance of laser at the position of the cube
        if (Input.GetKeyDown(KeyCode.Space))
            Instantiate(laser, transform.position, Quaternion.identity);
    }
}
