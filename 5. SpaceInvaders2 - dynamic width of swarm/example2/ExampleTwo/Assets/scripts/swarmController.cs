﻿using UnityEngine;
using System.Collections;

public class swarmController : MonoBehaviour {

    public int rows;
    public int columns;
    public float spacing;

    //variable to be used in the for loop
    public GameObject alien;

    gameController controller;


    //create a variable for the width and height of the swarm
    float width;
    float height;
	float edgeX;

	// Use this for initialization
	void Start () {
        controller = GameObject.FindGameObjectWithTag("GameController").GetComponent<gameController>();

        Vector3 topRight = controller.calculateEdge();

		edgeX = topRight.x;

        //place the object in the top left corner (the opposite side to top right)
        //move down to the bottom of the empty game object
        //position of the first alien, taking into account that we are using
        //the width of the alien
        transform.position = new Vector3(-topRight.x+(0.5f+spacing), topRight.y, 0);

        generateAliens();
        width = columns + (spacing * columns);
        height = rows + (spacing * rows);
   
        
	}

    void generateAliens()
    {
        //rows
        for (int r = rows-1; r >= 0; r--)
        {
            //columns
            for (int c = 0; c <= columns-1; c++)
            {
                GameObject tempAlien;
             

				Vector3 tempPos = new Vector3(transform.position.x+(c+c*spacing)+0.5f,transform.position.y-(r+r*spacing)-0.5f,0);

                tempAlien = (GameObject)Instantiate(alien, tempPos, Quaternion.identity);
                
                //set the parent of each alien to swarm
                tempAlien.transform.parent = this.transform;

                tempAlien.name = r + " " + c;
			
            }

        }

		//the width is actually the position of the last X

    }

	int dir;
    // Update is called once per frame
    void Update()
    {

        //travelling left, catering for the width of the swarm
        /*if (transform.position.x > edgeX)
        {
            dir = 1;
        }


        //travelling right, since our swarm controller is on the top left, he will stop correctly
        if (transform.position.x < -edgeX)
        {
            dir = -1;
        }

        transform.Translate(Vector3.right * 5f * dir * Time.deltaTime);*/

	}
}
