﻿using UnityEngine;
using System.Collections;

public class cubeController : MonoBehaviour {
       
    public Material[] colors;
    bool whiteblack = false;
    IEnumerator switchColors()
    {
        //we will switch the colors here
        while (true)
        {
            whiteblack = !whiteblack;
            yield return new WaitForSeconds(1f);
            if (whiteblack) setBlack();
            else setWhite();
        }  
    }
    public void setBlack()
    {
        GetComponent<Renderer>().sharedMaterial = colors[1];
    }

    public void setWhite()
    {
        GetComponent<Renderer>().sharedMaterial = colors[0];
    }

	// Use this for initialization
	void Start () {
        //StartCoroutine(switchColors());
	}

}
