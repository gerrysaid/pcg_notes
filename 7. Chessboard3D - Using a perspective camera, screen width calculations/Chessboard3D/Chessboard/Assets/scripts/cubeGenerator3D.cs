﻿using UnityEngine;
using System.Collections;

public class cubeGenerator3D : MonoBehaviour
{

		public GameObject myCube;
		Vector3 edge;


		// Use this for initialization
		void Start ()
		{
		edge = Camera.main.ViewportToWorldPoint (new Vector3 (1, 1, 10.5f));
		edge.x = edge.x * 2;
		edge.y = edge.y * 2;

		Debug.Log (edge.x + " " + edge.y);
				for (int rows = 0; rows<8; rows++) {
						for (int cols = 0; cols < 8; cols++) {
								GameObject tempCube = (GameObject)Instantiate (myCube,
                             new Vector3 (0, 0, 0), Quaternion.identity);
							tempCube.GetComponent<cubeController>().rotatex = true;
								if ((rows % 2) == 0) {
										if ((cols % 2) == 0)
												tempCube.GetComponent<cubeController> ().setWhite ();
										else
												tempCube.GetComponent<cubeController> ().setBlack ();
								} else {
										if ((cols % 2) == 0)
												tempCube.GetComponent<cubeController> ().setBlack ();
										else
												tempCube.GetComponent<cubeController> ().setWhite ();

								}
								tempCube.transform.parent = this.transform;
								tempCube.transform.localPosition = new Vector3 (cols + 0.5f, rows + 0.5f, 0);
						}
				}
				transform.position = new Vector3 (-4, -4, 10.5f);
				StartCoroutine (animate ());
		}

		bool scalebuttonpressed = true;
		bool animatebuttonpressed = false;

		void OnGUI ()
		{
				bool scaleButton = GUI.Button (new Rect (0, 0, 150, 50), "Scale");
				bool animateButton = GUI.Button (new Rect (0, 52, 150, 50), "Animate");

				if (scaleButton) {
						scalebuttonpressed = !scalebuttonpressed;
						if (!scalebuttonpressed)
								Scale ();
						else
								Reset ();
				}

				if (animateButton) {
						animatebuttonpressed = !animatebuttonpressed;
						
				}




		}

		void Reset ()
		{
				transform.position = new Vector3 (-4f, -4f, 10.5f);
				transform.localScale = new Vector3 (1f, 1f, 1f);
		}

		void Scale ()
		{
			
				Debug.Log (edge.x + " " + edge.y);
				float screenWidth = edge.x * 2;
				float screenHeight = edge.y * 2;

				float scalefactorx = screenWidth / 8;
				float scalefactory = screenHeight / 8;
				transform.position = new Vector3 (-edge.x, -edge.y, 10.5f);
				transform.localScale = new Vector3 (scalefactorx, scalefactory, 1f);

		}

	bool moveUp = true;
	bool moveDown = false;
	bool moveLeft = false;
	bool moveRight = false;

		IEnumerator animate ()
		{
				while (true) {
				if (animatebuttonpressed) {




				if (moveUp && transform.position.y < edge.y -8f) 
				{
					Debug.Log ("up");
					float newy = transform.position.y+0.1f;
					transform.position = new Vector3(transform.position.x,newy,transform.position.z);
				}
				else if (transform.position.y > edge.y -8f)
				{	
					Debug.Log ("done");
					Debug.Log (edge.x);
					moveUp = false;
					moveLeft = true;
				}
			
								
				if (moveLeft && transform.position.x > -edge.x) 
				{
					Debug.Log ("left");
					float newx = transform.position.x-0.1f;
					transform.position = new Vector3(newx,transform.position.y,transform.position.z);
				}
				else if (transform.position.x < -edge.x)
				{	
					moveLeft = false;
					moveDown = true;
				}


				if (moveDown && transform.position.y > -edge.y) 
				{
					Debug.Log ("down");
					Debug.Log (-edge.y);
					float newy = transform.position.y-0.1f;
					transform.position = new Vector3(transform.position.x,newy,transform.position.z);
				}
				else if (transform.position.y < -edge.y)
				{	
					moveDown = false;
					moveRight = true;
				}
			

				if (moveRight && transform.position.x < edge.x - 8f) 
				{
					Debug.Log ("right");
					float newx = transform.position.x+0.1f;
					transform.position = new Vector3(newx,transform.position.y,transform.position.z);
				}
				else if (transform.position.x > edge.x - 8f)
				{	
					moveRight = false;
					moveUp = true;
				}
								

		


								
						}
					yield return null;
				}
		}

}
