﻿using UnityEngine;
using System.Collections;

public class edgeFinder : MonoBehaviour {

	// Use this for initialization
	void Start () {
		//the edge of the viewport.  Bottom right is 0,0  Top left is 1,1
		Vector3? topRight = Camera.main.ViewportToWorldPoint (new Vector3(1,1,10f));


		if (topRight.HasValue) {
			GameObject.FindGameObjectWithTag ("sphere1").transform.position = 
				new Vector3 (-topRight.Value.x+0.5f, topRight.Value.y-0.5f, topRight.Value.z+0.5f);

			GameObject.FindGameObjectWithTag ("cube1").transform.position = 
				new Vector3 (topRight.Value.x-0.5f, topRight.Value.y-0.5f, topRight.Value.z+0.5f);


		}
	}
	

}

