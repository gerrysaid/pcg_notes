﻿using UnityEngine;
using System.Collections;

public class spinningCubeController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}


    void OnMouseDown()
    {
        Debug.Log("clicked");
    }

	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.back * 20f * Time.deltaTime);
        transform.Rotate(Vector3.up * 20f * Time.deltaTime);


        transform.Translate(Vector3.back * 10f * Time.deltaTime,Space.World);

        if (transform.position.z < -9)
            transform.position = new Vector3(0, 0, 0);
	}
}
