﻿using UnityEngine;
using System.Collections;

public class cubeController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        missedCounter = 0;
	}

    bool move = true;

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(1))
        {
            alternate *= -1;
        }
     
    }

    int missedCounter;

    float alternate = -1;
    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 100, 30), "missed cubes :" + missedCounter);
       
        GUIStyle style = new GUIStyle();
        style.normal.textColor = Color.white;
        style.fontStyle = FontStyle.Bold;
        

        GUI.Label(new Rect(0,30,100,30),"Screen Mouse: " +
            Input.mousePosition.x + " " +
            Input.mousePosition.y,style);

        Vector3 viewportCoords = Camera.main.ScreenToViewportPoint(
            new Vector3(Input.mousePosition.x,
                Input.mousePosition.y,5f));
        
        GUI.Label(new Rect(0, 60, 100, 30), "Viewport Mouse: " +
          viewportCoords.x + " " +
          viewportCoords.y, style);


        Vector3 worldCoords = Camera.main.ViewportToWorldPoint(
            viewportCoords);

        GUI.Label(new Rect(0, 80, 100, 30), "World Mouse: " +
        worldCoords.x + " " +
        worldCoords.y, style);

    }


    void OnTriggerEnter(Collider otherCube)
    {
        if (otherCube.tag == "blackCube")
        {
            Debug.Log("hit the other cube");
            //every time white cube hits black cube
            //make white cube switch direction
            alternate *= -1;
        }
    }

	// Update is called once per frame
	void Update () {
        transform.Rotate(Vector3.up * 20f * Time.deltaTime);
        transform.Rotate(Vector3.forward * 20f * Time.deltaTime);
      
            transform.Translate(Vector3.back * 6f * alternate * Time.deltaTime,Space.World);
       
        

        //goes away in the distance...
        if (transform.position.z > 0)
        {
            transform.position = new Vector3(0, 0, -10);
        }

        //missed!
        if (transform.position.z < -10)
        {
            missedCounter++;
         
            transform.position = new Vector3(alternate, 0, 0);
            transform.LookAt(Camera.main.transform.position);
        }
    }
}
