﻿using UnityEngine;
using System.Collections;

public class CoordinatesController : MonoBehaviour {

	void OnGUI()
	{
		Vector3 mouseScreenPosition = 
			Input.mousePosition;

		GUI.color = Color.white;
		//these coordinates are relative to the
		//bottom left corner of the screen
		GUI.Label (new Rect (0, 0, 250, 50),
		          "Sx: " + mouseScreenPosition.x +
				  " Sy: " + mouseScreenPosition.y);

		//init an empty position
		Vector3 mouseWorldPosition = new
			Vector3 (0, 0, 0);

		mouseWorldPosition = Camera.main.
			ScreenToWorldPoint (mouseScreenPosition);
		//these coordinates are relative to the
		//center of the screen
		GUI.Label (new Rect (0, 30, 250, 50),
		           "Wx: " + mouseWorldPosition.x +
		           " Wy: " + mouseWorldPosition.y);


	}


}
