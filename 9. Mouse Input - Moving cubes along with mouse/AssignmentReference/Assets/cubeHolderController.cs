﻿using UnityEngine;
using System.Collections;

public class cubeHolderController : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		//we need the world equivalent
		//of the mouse position

		Vector3 mouseWorldPosition = 
			Camera.main.ScreenToWorldPoint 
				(Input.mousePosition);

		//set the current position to the 
		//mouse world position
		//note: We are only interested 
		//in position.x and position.y 
		transform.position = new Vector3(
			mouseWorldPosition.x,mouseWorldPosition.y,0);
	}
}
