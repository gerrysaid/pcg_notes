﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class driver : MonoBehaviour {
	public float carSpeed=40f;
	public float steeringSpeed=100f;


	//new variables - total laps, laps done, total time
	public int totalLaps = 10;
	int lapsDone=0;
	float totalRaceTime=0f;

	//gameover
	bool gameover = false;


	float currentTime = 0f;
	//lap time
	float lapTime = 0f;
	float marginX;
	float marginY;

	float posX;
	float posY;

	bool startRace;
	
	float startTime;

	//number of laps
	// Use this for initialization
	void Start () {
		//put the car in the middle of the screen
		//transform.position = new Vector3 (0f, 0f, 0f);

		startTime = Time.time;

		//this is the value of can race when the light turns green.




	}

	void OnTriggerEnter2D(Collider2D otherObject)
	{
		if (otherObject.gameObject.name == "Finish") {
			//add 1 to laps done
			lapsDone++;

			lapTime = Mathf.Round(Time.time);
			totalRaceTime += currentTime;

			//Debug.Log (totalRaceTime);
			//update the text
			GameObject.Find ("LapsText").GetComponent<Text> ().text = lapsDone+"/"+totalLaps;

			if (lapsDone == totalLaps) {
				StartCoroutine (endGame ());
			}


		}

	}

	IEnumerator endGame()
	{
		gameover = true;
		GameObject.Find ("TotalTimeText").GetComponent<Text> ().text = totalRaceTime.ToString();
		yield return new WaitForSeconds (3f);
		SceneManager.LoadScene ("menu");
		//go back to the menu

	}


	//the coroutine that runs the timer
	IEnumerator Stopwatch()
	{
		while (gameover==false) {
			currentTime = Mathf.Round(Time.time-startTime);
			//reset current time
			currentTime = currentTime - lapTime;

			//what is this for?
			if (currentTime<10)
				GameObject.Find ("Stopwatch").GetComponent<Text> ().text = "0"+currentTime;
			else
				GameObject.Find ("Stopwatch").GetComponent<Text> ().text = currentTime.ToString();	
			yield return new WaitForSeconds (1f);
		}
		yield return null;


	}


	//Features I would like

	/*
	 * 1. Races have max 10 laps, show total time at the end in seconds
	 * 2. 3 different race tracks
	 * 3. 3 different cars with different speeds
	 * 4. A new game menu (with buttons)
	 * 
	 *
	 */

	bool gameStarted = false;

	// Update is called once per frame
	void Update () {
		//update the value of start race when its value changes
		startRace = GameObject.Find("trafficlightsGenerator").GetComponent<squareGeneratorScript>().canRace;

		if (startRace == true && gameStarted == false) {
			StartCoroutine (Stopwatch ());
			gameStarted = true;
		}

		if (gameStarted==true) {
			//reset the laps text
			GameObject.Find ("LapsText").GetComponent<Text> ().text = lapsDone + "/" + totalLaps;

			//invert the direction of rotation if reversing
			if (Input.GetAxis ("Vertical") < 0f) {
				steeringSpeed = -40f;
			} else {
				steeringSpeed = 40f;
			}

			//Debug.Log (steeringSpeed);
			if (gameover == false) {
				transform.Translate (new Vector3 (0f, 1f) * carSpeed * Input.GetAxis ("Vertical") * Time.deltaTime);

				transform.Rotate (new Vector3 (0f, 0f, 1f) * -steeringSpeed * Input.GetAxis ("Horizontal") * Time.deltaTime);
			}

			marginY = Camera.main.orthographicSize;
			//range in Y is between +10 and -10 (marginY)

			marginX = Camera.main.orthographicSize * Camera.main.aspect;

			posX = Mathf.Clamp (transform.position.x, -marginX, marginX);

			posY = Mathf.Clamp (transform.position.y, -marginY, marginY);

			transform.position = new Vector3 (posX, posY, 0f);
		}
		//Debug.Log (posX + " " + posY);
	
	}
}
