﻿using UnityEngine;
using System.Collections;

public class squareGeneratorScript : MonoBehaviour {

	public GameObject squareToGenerate;
	public int numberOfSquares = 3;
	public bool canRace = false;


	// Use this for initialization. Happens when I press play
	void Start () {
		int counter = 0;

		//create 3 boxes on top of each other, and group them together
		//into one object
		do {
			counter++;
			GameObject temp = Instantiate(squareToGenerate,new Vector3(0f,counter,1f),Quaternion.identity) as GameObject;
			temp.transform.parent = this.transform;
			temp.name = counter.ToString();
		} while(counter < numberOfSquares);

		//transform.position = GameObject.Find ("trafficlightsPosition").transform.position;
		//StartCoroutine (animatedBoxes ());
		StartCoroutine(trafficLights());
	}

	IEnumerator trafficLights(){
		//sequence of traffic lights
		//switch on red, then switch on red and amber, finally switch off red and amber and switch on green
		GameObject redLight = GameObject.Find("3");
		GameObject yellowLight = GameObject.Find ("2");
		GameObject greenLight = GameObject.Find ("1");

		redLight.GetComponent<SpriteRenderer> ().color = Color.red;
		yield return new WaitForSeconds (1f);
		yellowLight.GetComponent<SpriteRenderer> ().color = Color.yellow;
		yield return new WaitForSeconds (1f);
		redLight.GetComponent<SpriteRenderer> ().color = Color.white;
		yellowLight.GetComponent<SpriteRenderer> ().color = Color.white;
		greenLight.GetComponent<SpriteRenderer>().color = Color.green;
		yield return new WaitForSeconds (1f);
		canRace = true;
		yield return null;
	}


	IEnumerator animatedBoxes()
	{
		int counter = 0;
		//animation is going to go on forever
		while (true) {
			if (counter < 3) {
				counter++;
			} else {
				counter = 1;
			}
			//find the box you want
			GameObject.Find(counter.ToString()).GetComponent<SpriteRenderer>().color = Color.red;
			yield return new WaitForSeconds (1f);
			GameObject.Find(counter.ToString()).GetComponent<SpriteRenderer>().color = Color.white;
			yield return new WaitForSeconds (1f);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
