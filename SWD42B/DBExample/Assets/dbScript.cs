﻿using UnityEngine;
using UnityEngine.UI;
using System.Data;
using System.Collections;
using Mono.Data.Sqlite;

public class dbScript : MonoBehaviour {

	//a reference to the database connection
	IDbConnection databaseConnection;


	// Use this for initialization
	void Start () {
		GetData ();
	}

	//method to be linked with the button
	public void InsertButtonPressed()
	{
		string nameInput = GameObject.Find ("NameField").GetComponent<InputField> ().text;
		int scoreInput = int.Parse (GameObject.Find ("ScoreField").GetComponent<InputField> ().text);



		GameObject.Find ("NameField").GetComponent<InputField> ().text = "";
		GameObject.Find ("ScoreField").GetComponent<InputField> ().text = "";

		InsertData (nameInput, scoreInput);
		GetData ();

	}

	void InsertData(string name,int score)
	{
		IDbCommand insertCommand = databaseConnection.CreateCommand ();

		string sql = "insert into highscores(playername,score) values ('" + name + "'," + score + ")";
		Debug.Log (sql);
		insertCommand.CommandText = sql;
		//this means that there will be no result shown
		insertCommand.ExecuteNonQuery ();
	}

	//this method is going to query data from the database
	void GetData(){
		//generate a connection string to the database. The name at the end needs to correspond with 
		//the name of the database FILE
		string conn = "URI=file:" + Application.dataPath + "/gamehighscores.s3db"; //Path to database.

		//connect to the database directly
		databaseConnection = (IDbConnection) new SqliteConnection(conn);

		//open the database connection
		databaseConnection.Open();

		//create an SQL command to get data from the database
		IDbCommand selectCommand = databaseConnection.CreateCommand();

		//the text of the query command for the database
		string sql = "select playername,score from highscores order by score desc";

		//assign the sql to the command
		selectCommand.CommandText = sql;

		//read the data into a data reader
		IDataReader queryReader = selectCommand.ExecuteReader ();

		string output = "";

		while (queryReader.Read ()) {
			output += "Player name: " + queryReader.GetString (0) + " Score:" + queryReader.GetInt32 (1)+"\n";
		}

		GameObject.Find ("OutputText").GetComponent<Text> ().text = output;
	}

	
	// Update is called once per frame
	void Update () {
	
	}
}
