﻿using UnityEngine;
using System.Collections;

public class cardScript : MonoBehaviour {

	int index;
	public Sprite frontImage,backImage;


	bool flip = false;

	void OnClick(){
		if (flip) {
			GetComponent<SpriteRenderer> ().sprite = backImage;
			flip = !flip;
		}

	}


	// Use this for initialization
	void Start () {
		GetComponent<SpriteRenderer> ().sprite = frontImage;
	}
	

}
