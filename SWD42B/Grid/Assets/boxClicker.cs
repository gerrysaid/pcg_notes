﻿using UnityEngine;
using System.Collections;

public class boxClicker : MonoBehaviour {


	//click the mouse on the box
	bool clicked = false;
	//change this method so it flips the color from red to white
	void OnMouseDown(){
		//Debug.Log ("Mouse Clicked on " + this.name);
		//it's going to turn red
		if (!clicked) {
			this.GetComponent<SpriteRenderer> ().color = Color.red;
			clicked = true;
		} else {
			this.GetComponent<SpriteRenderer> ().color = Color.white;
			clicked = false;
		}
	}


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
