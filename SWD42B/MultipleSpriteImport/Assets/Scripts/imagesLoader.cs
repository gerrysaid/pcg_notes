﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class imagesLoader : MonoBehaviour {

	//a list of all the pictures
	public List<Sprite> allPictures;

	public GameObject tileToLoad;

	// Use this for initialization
	void Start () {
		//now we load the pictures from the resources folder
		allPictures.AddRange(Resources.LoadAll<Sprite>("Sprites/boxes") as Sprite[]); 

		//this has to happen in a loop, for however many cards and sprite pairings we have
		GameObject tile1 = Instantiate (tileToLoad, new Vector3 (0f, 0f), Quaternion.identity) as GameObject;
		//set the front image of tile 1 to the first element in the list
		tile1.GetComponent<tileController> ().setFront (allPictures [1]);
		//remove the picture selected from the deck
		allPictures.Remove(allPictures[1]);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
