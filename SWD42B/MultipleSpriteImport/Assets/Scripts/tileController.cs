﻿using UnityEngine;
using System.Collections;

public class tileController : MonoBehaviour {
	//save the image that the tile is instantiated with (back of card)
	public Sprite backImage,frontImage;

	bool clicked = false;

	void Start()
	{
		backImage = GetComponent<SpriteRenderer> ().sprite;

	}

	public void setFront(Sprite theImage)
	{
		frontImage = theImage;
	}

	void OnMouseDown()
	{
		if (clicked == false) {
			GetComponent<SpriteRenderer> ().sprite = frontImage;
			clicked = true;
		} else {
			GetComponent<SpriteRenderer> ().sprite = backImage;
			clicked = false;
		}

	}

}
