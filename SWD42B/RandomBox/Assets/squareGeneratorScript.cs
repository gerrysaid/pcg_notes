﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class squareGeneratorScript : MonoBehaviour {
	public GameObject squareToGenerate;
	//a reference to the square in the scene
	GameObject currentSquare;
	GameObject countdownText;

	public float timeGap;

	public bool onyourMarks;


	// Use this for initialization
	void Start () {
		countdownText = GameObject.Find ("CountDownText");

		countdownText.GetComponent<Text> ().text = "Time: " +timeGap.ToString ();

		//generate the square at zero rotation
		onyourMarks = false;

		StartCoroutine (startingGun ());

	}

	IEnumerator startingGun()
	{
		yield return new WaitForSeconds (5f);
		onyourMarks = true;
	}


	void Update(){
		if (onyourMarks) {
			currentSquare = Instantiate (squareToGenerate, randomSquarePositionOnScreen (), Quaternion.identity) as GameObject;
			//start the coroutine
			StartCoroutine (moveSquare ());
			onyourMarks = false;
		}
	}

	IEnumerator countDown(){
		int currentSeconds = 0;
		while (currentSeconds < timeGap) {
			currentSeconds++;
			countdownText.GetComponent<Text> ().text = "Time: " + (timeGap - currentSeconds).ToString ();
			yield return new WaitForSeconds (1f);
		}
		yield return null;
	}


	IEnumerator moveSquare(){
		while (true) {
			StartCoroutine (countDown ());
			yield return new WaitForSeconds (timeGap);
			currentSquare.transform.position = randomSquarePositionOnScreen ();


		}


	}

	//random position on screen
	public Vector3 randomSquarePositionOnScreen()
	{
		float randomX = 0f;
		float randomY = 0f;

		float topMost = Camera.main.orthographicSize;
		float bottomMost = -Camera.main.orthographicSize;

		float leftMost = -Camera.main.orthographicSize * Camera.main.aspect;
		float rightMost = Camera.main.orthographicSize * Camera.main.aspect;

		//the unity library for random
		//given an object scale of 1.  If the object scale was bigger we would need to divide it by 2
		randomY = Random.Range (bottomMost+0.5f, topMost-0.5f);

		randomX = Random.Range (leftMost+0.5f, rightMost-0.5f);

		return new Vector3 (randomX, randomY);
	}

	

}
