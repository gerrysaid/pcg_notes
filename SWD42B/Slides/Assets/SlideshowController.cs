﻿using UnityEngine;
using System.Collections;

public class SlideshowController : MonoBehaviour {

	//made the variables public so that they can be read from button controller

	//declare an array which contains 4 slots
	public string[] pictures = new string[4];

	public int counter = 0;

	// Use this for initialization
	void Start () {
		pictures [0] = "Slide1";
		pictures [1] = "Slide2";
		pictures [2] = "Slide3";
		pictures [3] = "Slide4";
	
	}
	
	// Update is called once per frame
	void Update () {


		//synchronize the counters
		counter = GameObject.Find ("EventSystem").GetComponent<buttonController> ().count;
	

		//GameObject.Find ("Slide3").GetComponent<SpriteRenderer> ().enabled = true;

		//PREVIOUS
		if (Input.GetKeyDown (KeyCode.A)) {
			//happens when A is pressed
			if (counter > 0) {
				counter = counter - 1;
				//counter--;
			} else {
				counter = 3;
			}

			Debug.Log (counter);

			//for each element in the array
			foreach(string slide in pictures)
			{
				Debug.Log (slide);
				GameObject.Find (slide).GetComponent<SpriteRenderer> ().enabled = false;
			}

			GameObject.Find (pictures [counter]).GetComponent<SpriteRenderer> ().enabled = true;

		}


		//NEXT
		if (Input.GetKeyDown (KeyCode.D)) {

			if (counter >= 3) {
				counter = 0;
			} else {
				counter = counter + 1;
				//counter++
			}

			//for each element in the array
			foreach(string slide in pictures)
			{
				Debug.Log (slide);
				GameObject.Find (slide).GetComponent<SpriteRenderer> ().enabled = false;
			}

			GameObject.Find (pictures [counter]).GetComponent<SpriteRenderer> ().enabled = true;


			
		}




	  
	}
}
