﻿using UnityEngine;
using System.Collections;

public class buttonController : MonoBehaviour {

	//made count public
	public int count=0;
	string[] pics;


	// Use this for initialization
	void Start () {
		pics = GameObject.Find ("Player").GetComponent<SlideshowController> ().pictures;
		count = GameObject.Find ("Player").GetComponent<SlideshowController> ().counter;
	}
	
	// Update is called once per frame
	void Update () {
		//get the pictures and the counter from the slideshow controller
	

	}

	public void animateButtonPressed(){
		//code to run when the animate button is pressed
		GameObject.Find("MySlideshow").GetComponent<SpriteRenderer>().enabled = true;
		GameObject.Find ("MySlideshow").GetComponent<Animator> ().SetTrigger ("StartSlideshow");
	}


	public void firstButtonPressed()
	{
		
		//for each element in the array
		foreach(string slide in pics)
		{
			//Debug.Log (slide);
			GameObject.Find (slide).GetComponent<SpriteRenderer> ().enabled = false;
		}

		//always show first image
		GameObject.Find (pics [0]).GetComponent<SpriteRenderer> ().enabled = true;
		//Debug.Log ("First");
	}
	public void previousButtonPressed()
	{
		Debug.Log ("Previous");

		//happens when A is pressed
		if (count > 0) {
			count--;
			//counter--;
		} else {
			count = pics.Length-1;
		}



		//for each element in the array
		foreach(string slide in pics)
		{
			Debug.Log (slide);
			GameObject.Find (slide).GetComponent<SpriteRenderer> ().enabled = false;
		}

		GameObject.Find (pics [count]).GetComponent<SpriteRenderer> ().enabled = true;

	}
	public void nextButtonPressed()
	{
		Debug.Log ("Next");

		if (count >= pics.Length-1) {
			count = 0;
		} else {
			count = count + 1;
			//counter++
		}

		//for each element in the array
		foreach(string slide in pics)
		{
			Debug.Log (slide);
			GameObject.Find (slide).GetComponent<SpriteRenderer> ().enabled = false;
		}

		GameObject.Find (pics [count]).GetComponent<SpriteRenderer> ().enabled = true;


	}

	public void lastButtonPressed()
	{
		//for each element in the array
		foreach(string slide in pics)
		{
			//Debug.Log (slide);
			GameObject.Find (slide).GetComponent<SpriteRenderer> ().enabled = false;
		}

		//always show last image
		GameObject.Find (pics [pics.Length-1]).GetComponent<SpriteRenderer> ().enabled = true;
	}

}
