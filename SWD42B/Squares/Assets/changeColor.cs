﻿using UnityEngine;
using System.Collections;

public class changeColor : MonoBehaviour {

	public Sprite front,back;


	bool clicked = false;


	void Start(){
		GetComponent<SpriteRenderer> ().sprite = back;
	}

	void OnMouseDown()
	{
		if (!clicked) {
			//GetComponent<SpriteRenderer> ().color = Color.red;
			GetComponent<SpriteRenderer> ().sprite = front;
			clicked = true;
		} else {
			//GetComponent<SpriteRenderer> ().color = Color.white;
			GetComponent<SpriteRenderer> ().sprite = back;
			clicked = false;
		}
	}

}
