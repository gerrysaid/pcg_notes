﻿using UnityEngine;
using System.Collections;
//the library for typed lists
using System.Collections.Generic;

public class gameControllerScript : MonoBehaviour {

	public GameObject squareToGenerate;

	List<GameObject> cardDeck;


	// Use this for initialization
	void Start () {
		cardDeck = new List<GameObject> ();
		int numcards = 2;
		int counter = 0;

		while (counter < numcards) {
			GameObject tempCard = 
				Instantiate (squareToGenerate, new Vector3 (counter, 0f), Quaternion.identity) as GameObject;

			cardDeck.Add (tempCard);
			counter++;
		}


	}
	

}
